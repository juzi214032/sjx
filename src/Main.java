import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入三角形层数：");
        final int row = sc.nextInt();
        int rowSpace = row;
        int rowStar = 1;
        for(int x = 0;x<row;x++) {//行数循环
            for (int i = rowSpace; i > 0; i--) {//循环空格
                System.out.print(" ");
            }
            for (int i = 0; i <rowStar; i++) {//循环*
                System.out.print("*");
            }
            System.out.println("");
            rowSpace = rowSpace - 1;
            rowStar = rowStar + 2;
        }
    }
}